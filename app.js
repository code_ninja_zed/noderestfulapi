// Dependencies
var express = require('express'),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    config = require('./config/env/local.js');

// Local Variables
var port = process.env.port || 3000;

// MongoDB
//var db = mongoose.connect(db.uri);
console.log(config.db.uri);

// Express
var app = express();
app.use(bodyParser.urlencoded({ extended:true }));
app.use(bodyParser.json());

// Router
var Book =  require('./models/bookModel');
bookRouter = require('./Routes/bookRoutes')(Book);
app.use('/api/Books', bookRouter);
//app.use('/api/Authors', authorRouter);

// Root route
app.get('/', function(req,res){
  res.send('Welcome to my API!');
});

app.listen(port, function(){
  console.log('Running on PORT: ' + port);
});
