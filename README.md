REpresentation State Transfer

The Constraints - Client Server
  Client ==> Request  ==> Server
  Server ==> Response ==> Client

The Constraints - Stateless Server
  Every the client request from the server
  should be in the client's request

The Constraints - Caching constraint

The Constraints -  Uniform Interface
  * Uniform form one server to the next
  * Resources (noun)
    Ex:
      Books http://../Book
      Authors http://../Author
  * HTTP verbs
    Get - Gets a single of list of resources
    Post - Adds a new resource
    Delete - Removes a resource
    Put - Update or replace a resource
    Patch - updates a piece of the resource

Hypermedia as the Engine of Application State (HATEOS)
