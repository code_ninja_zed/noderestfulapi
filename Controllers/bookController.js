var bookController = function(Book){

  var post = function(req,res){
    var book = new Book(req.body);

    if(!req.body.title){
      res.status(400)
      res.send('Title is required');
    }
    else {
      book.save(function(err){
        if(err) res.status(400).send(err);
        else res.status(201).send(book);
      });
    }
  }

  var get = function(req,res){
    Book.find(function(err, books){
      if(err) res.status(500).send(err);
      else res.status(200).json(books);
    });
  }

  return {
    post : post,
    get : get
  }
}

module.exports = bookController;
